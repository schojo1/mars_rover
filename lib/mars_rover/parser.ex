defmodule MarsRover.Parser do
  require Logger

  @command_patterns [
    init_world: ~r/^(?<x>\d+)\s(?<y>\d+)$/,
    operate_rover: ~r/^\((?<x>\d+),\s(?<y>\d+),\s(?<orientation>[NESW])\)\s(?<movements>[FLR]+)$/
  ]

  @doc """
  Parse a raw text into  set of instructions.

  ## Examples
    iex> MarsRover.Parser.parse("8 4\n(2, 3, E) LFR\n(4, 2, S) FFLF")
    {:ok,
    [
      [init_board: {8, 4}],
      [
        place_rover: {2, 3, :east},
        rotate: :left,
        move: :forward,
        rotate: :right
      ],
      [
        place_rover: {4, 2, :south},
        move: :forward,
        move: :forward,
        rotate: :left,
        move: :forward
      ]
    ]}
  """
  def parse(raw_instructions) when is_binary(raw_instructions) do
    raw_instructions
    |> String.trim()
    |> String.split("\n")
    |> parse_instructions()
  end

  def parse_instructions(instructions) do
    with {:ok, tokenized} <- tokenize_instructions(instructions),
         command_sets <- build_command_sets(tokenized) do
      {:ok, Enum.reverse(command_sets)}
    else
      {:error, {:bad_instruction, instruction}} ->
        Logger.error(
          "Couldn't parse instructions due to bad instruction" <>
            " in instruction set: #{inspect(instruction)}"
        )

        {:error, {:bad_instruction, instruction}}

      {:error, reason} ->
        Logger.error("Couldn't parse instructions due to error: #{reason}")
        {:error, reason}
    end
  end

  defp tokenize_instructions(instructions) do
    case do_tokenize_instructions(instructions) do
      {:error, reason} -> {:error, reason}
      tokenized -> {:ok, tokenized}
    end
  end

  defp do_tokenize_instructions(instructions) do
    instructions
    |> Enum.reduce_while(
      [],
      fn instruction, acc_parsed ->
        case tokenize_instruction(instruction) do
          :nomatch ->
            {:halt, {:error, {:bad_instruction, instruction}}}

          {:ok, parsed} ->
            {:cont, [parsed | acc_parsed]}
        end
      end
    )
  end

  defp tokenize_instruction(instruction) do
    @command_patterns
    |> Enum.find_value(:nomatch, fn {command_type, pattern} ->
      case Regex.named_captures(pattern, instruction) do
        nil -> nil
        %{} = command -> {:ok, {command_type, command}}
      end
    end)
  end

  defp build_command_sets(tokenized_instructions) do
    tokenized_instructions
    |> Enum.map(fn {action, data} -> make_command_set(action, data) end)
  end

  defp make_command_set(:init_world, %{"x" => x, "y" => y}) do
    [{:init_world, parse_position(x, y)}]
  end

  defp make_command_set(:operate_rover, %{
         "x" => x,
         "y" => y,
         "orientation" => orientation,
         "movements" => movements
       }) do
    [
      {:place_rover, parse_position(x, y, orientation)}
      | parse_movements(movements)
    ]
  end

  defp parse_position(x, y) do
    {String.to_integer(x), String.to_integer(y)}
  end

  defp parse_position(x, y, orientation) do
    {String.to_integer(x), String.to_integer(y), parse_orientation(orientation)}
  end

  defp parse_orientation("N"), do: :north
  defp parse_orientation("E"), do: :east
  defp parse_orientation("S"), do: :south
  defp parse_orientation("W"), do: :west

  defp parse_movements(movements) do
    movements
    |> String.codepoints()
    |> Enum.map(&make_movement_command/1)
  end

  defp make_movement_command("F"), do: {:move, :forward}
  defp make_movement_command("L"), do: {:rotate, :left}
  defp make_movement_command("R"), do: {:rotate, :right}
end
