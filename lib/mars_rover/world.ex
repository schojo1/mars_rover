defmodule MarsRover.World do
  @moduledoc """
  Operations for constructing and working with Worlds.

  The World is responsible for validating movements within it. A new %World{}
  can be constructed with World.new/2.
  """
  require Logger
  alias __MODULE__
  alias MarsRover.Position

  @enforce_keys [:min, :max]
  defstruct [:min, :max]

  @type t() :: %World{min: Position.t(), max: Position.t()}

  @doc """
  Construct a new %World{}.

  A World is assumed to be a 2-dimensional grid with minimum (i.e. lower-left)
  point (0, 0) and maximum (i.e. upper-right) point (`x`, `y`).
  """
  @spec new(
          x :: non_neg_integer(),
          y :: non_neg_integer()
        ) :: {:ok, t()} | {:error, term()}
  def new(x, y) do
    with {:ok, min} <- Position.new(0, 0),
         {:ok, max} <- Position.new(x, y) do
      {:ok, %World{min: min, max: max}}
    else
      {:error, reason} ->
        Logger.error("Couldn't create new World due to error: #{inspect(reason)}")
        {:error, reason}
    end
  end

  @doc """
  Check if a given %Position{} is within the bounds of the given %World{}.

  Returns :ok if position is inside the world, or :out_of_bounds otherwise.
  """
  @spec check_position(
          world :: t(),
          position :: Position.t()
        ) :: :ok | :out_of_bounds
  def check_position(%World{} = world, %Position{} = position) do
    case inside?(world, position) do
      true -> :ok
      false -> :out_of_bounds
    end
  end

  defp inside?(%World{min: min, max: max}, %Position{x: x, y: y}) do
    x >= min.x and y >= min.y and x <= max.x and y <= max.y
  end
end
