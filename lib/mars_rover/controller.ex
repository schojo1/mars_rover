defmodule MarsRover.Controller do
  @moduledoc """
  Controller for operating Rovers within a World.

  Ideally this would be a GenServer, but I ran out of time.
  """
  require Logger
  alias MarsRover.{Parser, Rover, World}

  @doc """
  Process a raw command string, e.g. from a file.

  ## Examples
  Pass `[print: true]` to get formatted output:

    iex> raw_command = File.read!("./input02.txt")
    "4 8\n(2, 3, N) FLLFR\n(1, 0, S) FFRLF\n"
    iex> raw_command |> MarsRover.Controller.process_commands(print: true)
    (2, 3, W)
    (1, 0, S) LOST

  By default, the state is returned:

    iex> raw_command |> MarsRover.Controller.process_commands()
    %{
      rovers: [
        %MarsRover.Rover{
          lost: false,
          orientation: :west,
          position: %MarsRover.Position{x: 2, y: 3}
        },
        %MarsRover.Rover{
          lost: true,
          orientation: :south,
          position: %MarsRover.Position{x: 1, y: 0}
        }
      ],
      world: %MarsRover.World{
        max: %MarsRover.Position{x: 4, y: 8},
        min: %MarsRover.Position{x: 0, y: 0}
      }
    }
  """
  def process_commands(raw_commands, opts \\ [])
  def process_commands(raw_commands, opts) when is_binary(raw_commands) do
    with {:ok, command_sets} <- Parser.parse(raw_commands),
         final_state <- process_command_sets(command_sets, %{world: nil, rovers: []}) do
      case opts[:print] do
        true -> print_result(final_state)
        _ -> final_state
      end
    else
      {:error, reason} ->
        Logger.error("Failed to process commands due to error: #{inspect(reason)}")
        {:error, reason}
    end
  end

  defp process_command_sets(command_sets, state) do
    command_sets
    |> Enum.reduce_while(state, fn command_set, acc_state ->
      case process_command_set(command_set, acc_state) do
        {:ok, new_state} ->
          {:cont, new_state}

        {:error, reason} ->
          Logger.error(
            "Failed to process command set: #{inspect(command_set)} with" <>
              " state: #{inspect(state)} due to reason: #{inspect(reason)}"
          )

          {:halt, {:error, reason}}
      end
    end)
  end

  defp process_command_set([{:init_world, {x, y}}], %{world: nil} = state) do
    with {:ok, world} <- World.new(x, y) do
      Logger.info("Initialized new world: #{inspect(world)}")
      {:ok, put_in(state.world, world)}
    end
  end

  defp process_command_set([{:init_world, _data} = command], state) do
    Logger.error(
      "Cannot reinitialize already existing world: #{inspect(state)} command: #{inspect(command)}"
    )

    {:error, :world_already_initialized}
  end

  defp process_command_set([{:place_rover, _} = command | _], %{world: nil} = state) do
    Logger.error(
      "Cannot place rover because there is no world in which to place it." <>
        " command: #{inspect(command)} state: #{inspect(state)}"
    )

    {:error, :world_does_not_exist}
  end

  defp process_command_set(
         [{:place_rover, {x, y, orientation}} | commands],
         %{world: world} = state
       ) do
    with {:ok, rover} <- Rover.new(x, y, orientation),
         :ok <- World.check_position(state.world, rover.position),
         {:ok, rover} = operate_rover(rover, world, commands) do
      {:ok, update_in(state.rovers, &(&1 ++ [rover]))}
    else
      :out_of_bounds ->
        Logger.warn(
          "Rover was placed out of bounds! World: #{inspect(world)}" <>
            " Rover placement: #{inspect({x, y, orientation})}"
        )

        {:ok, state}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp operate_rover(rover, _world, _commands = []) do
    {:ok, rover}
  end

  defp operate_rover(rover, world, [{:rotate, direction} | commands]) do
    case Rover.rotate(rover, direction) do
      %Rover{} = updated_rover ->
        Logger.debug(
          "Rover #{inspect(rover)} rotated #{inspect(direction)}." <>
            " New rover state: #{inspect(updated_rover)}"
        )

        operate_rover(updated_rover, world, commands)

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp operate_rover(rover, world, [{:move, direction} | commands]) do
    with %Rover{} = updated_rover <- Rover.move(rover, direction),
         :ok <- World.check_position(world, updated_rover.position) do
      Logger.debug(
        "Rover #{inspect(rover)} moved #{inspect(direction)}" <>
          " New rover state: #{inspect(updated_rover)}"
      )

      operate_rover(updated_rover, world, commands)
    else
      :out_of_bounds ->
        Logger.warn(
          "Rover #{inspect(rover)} moved #{inspect(direction)}" <>
            " and is now out of bounds!"
        )

        {:ok, Rover.lost(rover)}

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp print_result(%{rovers: rovers}) do
    rovers |> Enum.map(&to_string/1) |> Enum.join("\n") |> IO.puts
  end
end
