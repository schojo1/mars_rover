defmodule MarsRover.Rover do
  require Logger
  alias __MODULE__
  alias MarsRover.Position

  @orientations [:north, :east, :south, :west]
  @rotate_directions [:left, :right]

  @enforce_keys [:position, :orientation, :lost]
  defstruct [:position, :orientation, :lost]

  @type t() :: %Rover{
          position: Position.t(),
          orientation: orientation(),
          lost: boolean()
        }
  @type orientation() :: :north | :east | :south | :west
  @type rotate_direction() :: :left | :right
  @type move_direction() :: :forward

  @doc """
  Creates a new %Rover{} struct.
  """
  def new(x, y, orientation) when orientation in @orientations do
    with {:ok, position} <- Position.new(x, y) do
      {:ok,
       %Rover{
         position: position,
         orientation: orientation,
         lost: false
       }}
    else
      {:error, reason} ->
        Logger.error(
          "Failed to create rover at " <>
            "#{inspect({x, y, orientation})} due to error: #{inspect(reason)}"
        )
    end
  end

  def new(_x, _y, _orientation) do
    {:error, :invalid_data}
  end

  @doc """
  Mark a rover as lost.

  Once a rover is lost, it can no longer accept commands.
  """
  @spec lost(t()) :: t()
  def lost(%Rover{} = rover) do
    put_in(rover.lost, true)
  end

  @doc """
  Move the rover in a given direction. Currently only :forward is accepted.

  If the rover is lost, {:error, :rover_unreachable} is returned.
  """
  def move(%Rover{lost: true}, _direction) do
    {:error, :rover_unreachable}
  end

  def move(%Rover{orientation: :north} = rover, :forward) do
    update_in(rover.position.y, &(&1 + 1))
  end

  def move(%Rover{orientation: :east} = rover, :forward) do
    update_in(rover.position.x, &(&1 + 1))
  end

  def move(%Rover{orientation: :south} = rover, :forward) do
    update_in(rover.position.y, &(&1 - 1))
  end

  def move(%Rover{orientation: :west} = rover, :forward) do
    update_in(rover.position.x, &(&1 - 1))
  end

  defimpl String.Chars, for: Rover do
    def to_string(%Rover{position: pos, orientation: orientation, lost: lost}) do
      orientation = orientation |> Atom.to_string() |> String.first() |> String.capitalize()
      lost_str = if lost, do: " LOST", else: ""
      "(#{pos.x}, #{pos.y}, #{orientation})#{lost_str}"
    end
  end

  @doc """
  Rotate the rover :left or :right.
  """
  @spec rotate(
          t(),
          rotate_direction()
        ) :: t() | {:error, :rover_unreachable | :invalid_direction}
  def rotate(%Rover{lost: true}, _direction) do
    {:error, :rover_unreachable}
  end

  def rotate(%Rover{} = rover, direction) when direction in @rotate_directions do
    update_in(rover.orientation, &new_rotation(direction, &1))
  end

  def rotate(_rover, _direction) do
    {:error, :invalid_direction}
  end

  defp new_rotation(:right, :north), do: :east
  defp new_rotation(:right, :east), do: :south
  defp new_rotation(:right, :south), do: :west
  defp new_rotation(:right, :west), do: :north
  defp new_rotation(:left, :north), do: :west
  defp new_rotation(:left, :west), do: :south
  defp new_rotation(:left, :south), do: :east
  defp new_rotation(:left, :east), do: :north
end
