defmodule MarsRover.Position do
  require Logger

  @moduledoc """
  Functions for working with the %Position{} struct representing (X, Y)
  coordinates on a Cartesian plane.

  A %Position{} can be constructed using Position.new/2.
  """

  alias __MODULE__

  @type t() :: %Position{x: pos_integer(), y: pos_integer()}

  @enforce_keys [:x, :y]
  defstruct [:x, :y]

  @doc """
  Construct a new %Position{} struct from coordinates `x` and `y`.

  Creating a %Position{} with `x` or `y` coordinates < 0 produces an error.
  """
  def new(x, y) when is_integer(x) and x >= 0 and is_integer(y) and y >= 0 do
    {:ok, %Position{x: x, y: y}}
  end

  def new(x, y) do
    Logger.error(
      "Couldn't create Position " <>
        "due to invalid coordinates (#{inspect(x)}, #{inspect(y)})"
    )

    {:error, :invalid_coordinates}
  end
end
