defmodule MarsRover do
  @moduledoc """
  Documentation for `MarsRover`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MarsRover.hello()
      :world

  """
  def hello do
    :world
  end
end
