# MarsRover

MarsRover coding exercise.

Models a world as a 2D grid, and processes commands to move rovers around within
that world.

## Running the app
The application doesn't come with any UI, so it has to be operated from within
`iex`.

The app is stateless, so all commands must be passed at once as a single string.
Example command sequences are saved in `input01.txt` and `input02.txt`.

Start `iex` from the root of this repository:

``` sh
iex -S mix
```

Alias the Controller for convenience's sake:

``` elixir
iex(1)> alias MarsRover.Controller
MarsRover.Controller

```

*Note:* The logging is quite verbose and results can get lost in the output. You
may want to reduce the log level to reduce the amount of output:

``` elixir
iex(2)> Logger.configure(level: :critical)
```

Load a command sequence from `input01.txt`:

```elixir
iex(3)> raw_commands = File.read!("input01.txt")
"4 8\n(2, 3, E) LFRFF\n(0, 2, N) FFLFRFF\n"
```

Pass the raw command sequence to `Controller.process_commands/2`, with option
`[print: true]` to print output in human-friendly format:

``` elixir
iex(4)> raw_commands |> Controller.process_commands(print: true)
(4, 4, E)
(0, 4, W) LOST
:ok
```

Alternatively, pass `[print: false]` (or omit it entirely) to instead return the
application's state representation:

``` elixir
iex(5)> raw_commands |> Controller.process_commands()
%{
  rovers: [
    %MarsRover.Rover{
      lost: false,
      orientation: :east,
      position: %MarsRover.Position{x: 4, y: 4}
    },
    %MarsRover.Rover{
      lost: true,
      orientation: :west,
      position: %MarsRover.Position{x: 0, y: 4}
    }
  ],
  world: %MarsRover.World{
    max: %MarsRover.Position{x: 4, y: 8},
    min: %MarsRover.Position{x: 0, y: 0}
  }
}
```

## How it works
The raw command string is parsed by `MarsRover.Parser.parse/1` into a format
that the `MarsRover.Controller` can work with. The command string is parsed into
a list of command sets:

``` elixir
iex(6)> {:ok, parsed} = Parser.parse(raw_commands)
{:ok,
 [
   [init_world: {4, 8}],
   [
     place_rover: {2, 3, :east},
     rotate: :left,
     move: :forward,
     rotate: :right,
     move: :forward,
     move: :forward
   ],
   [
     place_rover: {0, 2, :north},
     move: :forward,
     move: :forward,
     rotate: :left,
     move: :forward,
     rotate: :right,
     move: :forward,
     move: :forward
   ]
 ]}
```

The first command set is typically an `:init_world` command, which only contains
one command:

``` elixir
iex(7)> hd(parsed)
[init_world: {4, 8}]
```

The following command sets are for the rovers: 

``` elixir
iex(8)> hd(tl(parsed))
[
  place_rover: {2, 3, :east},
  rotate: :left,
  move: :forward,
  rotate: :right,
  move: :forward,
  move: :forward
]
```

The first command in the command set describes where to place the rover, and the
subsequent commands describe how to operate the rover.

The `Controller` processes the commands sequentially to initialize a `World`
(responsible for checking if a position is within its bounds) and `Rovers`,
which maintain their own `Positions` (structs representing XY coordinates),
orientations, and status (lost vs. not lost).


## Limitations
The current implementation works only with an entire command string - the
command string must include initialization of the World (e.g. `4 8`) followed by
all the rover placement and operation commands (e.g. `(2, 3, E) LFRFF`).

If any of the commands in the command string are invalid, the whole operation
fails with a parse error:

``` elixir
# Setting orientation to `Z` in the second rover command 
iex(9)> bad_command_string = "4 8\n(2, 3, E) LFRFF\n(0, 2, Z) FFLFRFF\n"
"4 8\n(2, 3, E) LFRFF\n(0, 2, Z) FFLFRFF\n"

# Processing fails due to a bad instruction
iex(10)> bad_command_string |> Controller.process_commands
{:error, {:bad_instruction, "(0, 2, Z) FFLFRFF"}}
```

This implementation means that the application isn't interactive.

## Retrospective
The whole application -- in its current state -- took around 5 hours to
implement, including a break in the middle. This is admittedly than the
prescribed 2-3 hours, but I am still relatively new to Elixir, so it was worth
the time spent in my opinion.

My goal was to separate input from internal representation and processing, which
I think the parser does a good job of. Representing the World and the Rovers as
structs means that their functionality can be extended later on if the
requirements were to change.

The testing is largely missing -- this is due to time constraints. I tend to
code first, then write tests the tests to iron out creases in the code; as such,
there may be some bugs here and there.

Given more time I would add unit tests for each of the modules. I would also
like to decouple the operating logic from the Controller -- currently it's
responsible for taking the input, parsing it, and then processing the commands
in sequence, validating which actions can be performed given the current state.
For example, the `Controller` will return an error if an attempt is made to
place a `Rover` before the `World` has been initialized:

``` elixir
iex(11)> "(2, 3, E) LFRFF\n(0, 2, E) FFLFRFF\n" |> Controller.process_commands
{:error, :world_does_not_exist}
```

Ideally the `Controller` would just be responsible for accepting the command,
processing it via some other module, and then updating the state. To this end
the `Controller` would also ideally be a GenServer. I started the implementation
with this in mind and with the view of converting it to a GenServer later on,
but I ran out of time.

There would also ideally be a separate API layer between user input and the
Controller. The API layer would handle parsing the input and then pass the
commands to the GenServer, returning the result of the command evaluation. The
benefits of using a GenServer for the Controller is that it would let us send
commands separately, since the state could be persisted in the process's memory.

## Final notes
Elixir is a lot of fun, and I had a good time with this exercise. I think I will
continue to work on it as a learning exercise.
