defmodule MarsRover.PositionTest do
  use ExUnit.Case

  alias MarsRover.Position

  test "is constructed when x and y are integers >=0" do
    assert Position.new(3, 4) == {:ok, %Position{x: 3, y: 4}}
    assert Position.new(5, 9) == {:ok, %Position{x: 5, y: 9}}
  end

  test "error returned when x and y are invalid" do
    assert Position.new(-1, 4) == {:error, :invalid_coordinates}
    assert Position.new(1, -4) == {:error, :invalid_coordinates}
    assert Position.new("a", 4) == {:error, :invalid_coordinates}
    assert Position.new(1, "a") == {:error, :invalid_coordinates}
  end
end
